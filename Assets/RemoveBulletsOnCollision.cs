﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveBulletsOnCollision : MonoBehaviour
{
    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Hit something.");
        if (col.gameObject.CompareTag("Level"))
        {
            Debug.Log("Hit wall");
            Destroy(this);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered");
    }
}
