﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
    public float Speed = 10;
    public bool MANUAL_FIRE = false;

    private Rigidbody2D _rigidBody;

	// Use this for initialization
	public void Start () {
        
    }

    public void Fire(float direction)
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        Vector3 scale = transform.localScale;
        scale.x = direction;
        transform.localScale = scale;

        _rigidBody.velocity = transform.right * Speed * direction;
    }
	
	// Update is called once per frame
	public void Update () {
		if(MANUAL_FIRE)
        {
            Fire(1f);
            MANUAL_FIRE = false;
        }
	}

    public void FixedUpdate()
    {

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Level"))
        {
            Debug.Log("Hit wall");
            Destroy(gameObject);
        }
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log("Hit player!");
            Destroy(gameObject); // destroy the bullet
            PlayerController player = col.gameObject.GetComponent<PlayerController>();
            player.Die();
        }
    }

}
