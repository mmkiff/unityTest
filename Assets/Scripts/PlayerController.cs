﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    [SyncVar(hook = "OnStateChange")]
    public PlayerState STATE;

    [SyncVar(hook = "OnDirectionChange")]
    public Direction DIRECTION = Direction.Right;

    [SyncVar]
    public int DEATH_COUNT = 0;

    [SyncVar]
    public bool IS_ALIVE = true;

    public float MOVE_FORCE = 365;
    public float MAX_SPEED = 5f;
    public float FIRE_RATE = 0.3f;
    public GameObject BULLET;
    public Transform SHOT_SPAWN;
    public Transform GROUND_CHECK;
    public float JUMP_FORCE = 1000f;

    private Rigidbody2D _rigidBody;
    private float _nextFire;
    private bool _isGrounded = false;
    private bool _doJump = false;    
    private Animator _animator;

    public void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    public void Update()
    {
        LocalUpdate();
    }

    private void OnStateChange(PlayerState state)
    {
        STATE = state;

        if (state == PlayerState.Idle)
            _animator.SetTrigger("playerIdle");
        else if (state == PlayerState.Jumping)
            _animator.SetTrigger("playerJump");
        else if (state == PlayerState.Walking)
            _animator.SetTrigger("playerWalk");
        else if (state == PlayerState.Die)
            _animator.SetTrigger("playerDie");
    }

    private void OnDirectionChange(Direction direction)
    {
        DIRECTION = direction;

        Vector3 theScale = transform.localScale;
        theScale.x = (float)direction;
        transform.localScale = theScale;
    }

    private void LocalUpdate()
    {
        if (!isLocalPlayer || !IS_ALIVE)
            return;

        _isGrounded = Physics2D.Linecast(transform.position, GROUND_CHECK.position, 1 << LayerMask.NameToLayer("Ground")); // Shouldn't this be in fixed update?

        if (Input.GetButton("Fire1"))
            CmdShoot();
        if (Input.GetButtonDown("Jump") && _isGrounded)
            _doJump = true; // seems weird to do it this way. why not just move to fixedUpdate and call jump() directly
    }

    public void FixedUpdate()
    {
        if (!isLocalPlayer || !IS_ALIVE)
            return;

        Vector3 previousPosition = transform.position;

        PlayerState previousState = STATE;
        Direction previousDirection = DIRECTION;
        Direction newDirection = DIRECTION;

        PlayerState newState = PlayerState.Idle;

        float horizontalMovement = Input.GetAxis("Horizontal");

        if (horizontalMovement * _rigidBody.velocity.x < MAX_SPEED)
            _rigidBody.AddForce(Vector2.right * horizontalMovement * MOVE_FORCE);

        if (Mathf.Abs(_rigidBody.velocity.x) > MAX_SPEED)
            _rigidBody.velocity = new Vector2(Mathf.Sign(_rigidBody.velocity.x) * MAX_SPEED, _rigidBody.velocity.y);

        if (horizontalMovement > 0)
            newDirection = Direction.Right;
        else if (horizontalMovement < 0)
            newDirection = Direction.Left;

        if (horizontalMovement != 0)
            newState = PlayerState.Walking;

        if (!_isGrounded)
            newState = PlayerState.Jumping;

        // seems like we don't sync up properly when the player is idle so the animation isn't coming across. Maybe do a movement check and apply idle if no movement

        if (_doJump && _isGrounded)
        {
            _rigidBody.AddForce(new Vector2(0f, JUMP_FORCE));
            _doJump = false;
        }

        if (newDirection != previousDirection)
            CmdUpdateDirection(newDirection);

        if (newState != previousState)
            CmdUpdateState(newState);
    }

    [Command]
    private void CmdUpdateState(PlayerState state)
    {
        STATE = state;
    }

    [Command]
    private void CmdUpdateDirection(Direction direction)
    {
        DIRECTION = direction;
        //Vector3 theScale = transform.localScale;
        //theScale.x = (float)direction;
        //transform.localScale = theScale;
    }

    [Command] // command means it only gets run on the server. 
    public void CmdShoot()
    {
        if (Time.time > _nextFire)
        {
            _nextFire = Time.time + FIRE_RATE;
            GameObject bullet = Instantiate(BULLET, SHOT_SPAWN.position, SHOT_SPAWN.rotation);
            bullet.SendMessage("Fire", (float)DIRECTION);
            NetworkServer.Spawn(bullet);

            // Destroy the bullet after 10 seconds
            Destroy(bullet, 10.0f);
        }
    }

    public void Die()
    {
        IS_ALIVE = false;
        Debug.Log("Player died.");
        DEATH_COUNT++;
        CmdUpdateState(PlayerState.Die);
        //StartCoroutine("Respawn");
        Invoke("Respawn", 5.0f);
        // wait a bit
        // respawn
    }

    private void Respawn()
    {
        //yield return new WaitForSeconds(5f);

        IS_ALIVE = true;
        CmdUpdateState(PlayerState.Idle);
    }
}

public enum PlayerState
{
    Idle = 0,
    Walking = 1,
    Jumping = 2,
    Die = 3
}

public enum Direction
{
    Left = -1,
    Right = 1
}